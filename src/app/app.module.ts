import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomepageComponent } from './features/homepage.component';
import { UsersComponent } from './features/users/users.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './core/navbar/navbar.component';
import { SettingsComponent } from './features/settings.component';
import { UsersPageComponent } from './features/users/users-page.component';
import { UsersService } from './features/users/users.service';


@NgModule({
  declarations: [
    AppComponent, HomepageComponent, UsersComponent, NavbarComponent, SettingsComponent, UsersPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

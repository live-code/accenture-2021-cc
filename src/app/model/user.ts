import { Gender } from './gender';

export interface User {
  id: number;
  name?: string;
  age: number;
  gender: Gender;
  city?: string;
  birthday: number;
  bitcoins: number
}

const MALE = 'M'
const FEMALE = 'F'
export type Gender = typeof MALE | typeof FEMALE;

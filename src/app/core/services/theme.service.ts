import { Injectable } from '@angular/core';

type Themes = 'dark' | 'light';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  value: Themes = 'dark';

  setTheme(value: Themes): void {
    this.value = value;
  }

}

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div 
      style="padding: 10px"
      [style.background-color]="themeService.value === 'dark'? '#222' : '#ccc'"
    >
      <button class="btn btn-outline-primary" routerLink="home" routerLinkActive="bg-warning">home</button>
      <button class="btn btn-outline-primary" routerLink="users" routerLinkActive="bg-warning">users</button>
      <button class="btn btn-outline-primary" routerLink="settings" routerLinkActive="bg-warning">settings</button>
    </div>
  `
})
export class NavbarComponent {
  constructor(public themeService: ThemeService) {}
}

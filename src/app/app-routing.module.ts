import { NgModule } from '@angular/core';
import { HomepageComponent } from './features/homepage.component';
import { RouterModule } from '@angular/router';
import { SettingsComponent } from './features/settings.component';
import { UsersPageComponent } from './features/users/users-page.component';

const routes = [
  { path: 'home', component: HomepageComponent },
  { path: 'users', component: UsersPageComponent },
  { path: 'settings', component: SettingsComponent },
  { path: '', component: HomepageComponent},
  // { path: '**', component: Page404Component },
  { path: '**', redirectTo: 'home'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}

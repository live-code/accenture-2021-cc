import { Component } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  providers: [
    // UsersService,
    { provide: UsersService, useClass: UsersService }
  ],
  template: `
    
    <form
      #f="ngForm" (submit)="userService.save(f)"
      style="padding: 10px; border: 4px solid white"
    >
      <div *ngIf="inputName.errors?.required">Campo Obbligatorio</div>
      <div *ngIf="inputName.errors?.minlength">
        Minimo 3 caratteri.
        Mancano {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength}} chars
      </div>
      <input
        class="form-control"
        [ngClass]="{ 'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid }"
        [ngModel]="userService.activeUser?.name"
        #inputName="ngModel"
        name="name"
        type="text"
        required
        minlength="3"
        placeholder="Name"
      >
    
      <input
        class="form-control"
        type="text" name="city" 
        [ngModel]="userService.activeUser?.city" 
        placeholder="City">
      
      <select
        class="form-control"
        [ngClass]="{ 'is-invalid': inputGender.invalid && f.dirty, 'is-valid': inputGender.valid }"
        name="gender"
        #inputGender="ngModel"
        [ngModel]="userService.activeUser?.gender"
        required
      >
        <option [ngValue]="null">Select gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      
      <div class="btn-group">
        <button type="submit" class="btn btn-primary" [disabled]="f.invalid">
          {{userService.activeUser ? 'EDIT' : 'ADD'}}
        </button>
        
        <button 
          type="button" 
          [disabled]="!userService.activeUser"
          class="btn btn-outline-primary" (click)="userService.clear(f)">CLEAR</button>
      </div>
    </form>
    
    <div class="container mt-3">
      <div
        class="list-group-item"
        *ngFor="let user of userService.users"
        [ngClass]="{'active': user.id === userService.activeUser?.id}"
        (click)="userService.setActiveUser(user)"
      >

       {{user.name}}
        
        <i class="fa fa-trash pull-right" (click)="userService.deleteUser(user, $event)"></i>

      </div>
    </div>
    
  `,
})
export class UsersComponent {
  constructor( public userService: UsersService) {
    this.userService.getAllUsers();
  }

}


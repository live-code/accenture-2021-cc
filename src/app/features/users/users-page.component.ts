import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users-page',
  template: `
    
    <div class="row">
      <div class="col">
        <app-users></app-users>
      </div>
      <div class="col">
        <app-users></app-users>
      </div>
    </div>
  `,

  styles: [
  ]
})
export class UsersPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

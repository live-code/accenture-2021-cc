import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../../model/user';
import { NgForm } from '@angular/forms';
import { environment } from '../../../environments/environment';

@Injectable()
export class UsersService {
  users: User[];
  error: HttpErrorResponse;
  activeUser: User;

  constructor(private http: HttpClient) {
  }

  getAllUsers(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(
        result => this.users = result,
        error => this.error = error
      );
  }


  save(form: NgForm) {
    const a = environment.production
    if (this.activeUser) {
      this.edit(form);
    } else {
      this.add(form)
    }
  }


  edit(form: NgForm) {
    this.http.patch<User>(`http://localhost:3000/users/${this.activeUser.id}`, form.value)
      .subscribe(result => {
        const index = this.users.findIndex(u => u.id === this.activeUser.id);
        this.users[index] = result;
      });
  }

  add(form: NgForm): void {
    this.http.post<User>('http://localhost:3000/users', form.value)
      .subscribe(result => {
        this.users.push(result);
        form.reset();
      });
  }

  deleteUser(user: User, event: MouseEvent): void {
    event.stopPropagation();
    this.http.delete(`http://localhost:3000/users/${user.id}`)
      .subscribe(() => {
        this.users = this.users.filter(u => u.id !== user.id);

        if (this.activeUser && this.activeUser.id === user.id) {
          this.activeUser = null;
        }
      })
  }

  setActiveUser(user: User): void {
    this.activeUser = user;
  }


  clear(form: NgForm): void {
    form.reset();
    this.activeUser = null;
  }

}

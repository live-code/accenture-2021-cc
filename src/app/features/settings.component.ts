import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <button (click)="themeService.setTheme('dark')">Dark</button>
    <button (click)="themeService.setTheme('light')">light</button>
  `,
})
export class SettingsComponent {

  constructor(public themeService: ThemeService) {
    console.log(themeService.value)
  }

}

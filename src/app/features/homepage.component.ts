import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { gsap, Bounce, Back } from 'gsap';

@Component({
  selector: 'app-homepage',
  template: `
    <h1>home</h1>
    
    <div class="box" #box (click)="back()">ciao ciao</div>
    
  `,
  styles: [`
    .box {
      background-color: red;
    }
  `]
})
export class HomepageComponent implements AfterViewInit {
  @ViewChild('box') panel: ElementRef<HTMLDivElement>;

  ngAfterViewInit(): void {
    gsap.to(this.panel.nativeElement, {
      opacity: 0.2, background: 'green', height: '200px', scale: 0.5, rotate: 90, duration: 2.5,
      ease: Back.easeOut
    });
  }

  back(): void {
    gsap.to(this.panel.nativeElement, {
      opacity: 0.2, background: 'red', height: '50px', duration: 1.5,
      ease: Back.easeOut
    });
  }
}
